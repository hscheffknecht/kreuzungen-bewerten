package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Dictionary;

public interface IDbEntity {
    void loadData(ResultSet resultSet) throws SQLException;

    void saveData(Dictionary<String, String> data);

    Integer getId();
    void setId(Integer id);
}