package entity;

import database.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Dictionary;

public class Rating implements IDbEntity {
    Integer id = 0;
    Integer userId;
    Date createdOn;
    String text;
    Integer rating;
    Integer crossId;

    @Override
    public void loadData(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.userId = resultSet.getInt("user_id");
        this.createdOn = resultSet.getDate("created_on");
        this.text = resultSet.getString("text");
        this.rating = resultSet.getInt("rating");
        this.crossId = resultSet.getInt("cross_id");
    }

    @Override
    public void saveData(Dictionary<String, String> data) {
        data.put("user_id", this.userId.toString());
        data.put("created_on", (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(this.createdOn));
        data.put("text", this.text);
        data.put("rating", this.rating.toString());
        data.put("cross_id", this.crossId.toString());
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        Database db = new Database();
        User user = db.readOneEntity(User.class, "id=" + this.getUserId());
        return user.userName;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedOnFormatted() {
        return (new SimpleDateFormat("dd.MM.yyyy")).format(this.createdOn);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getCrossId() {
        return crossId;
    }

    public void setCrossId(Integer crossId) {
        this.crossId = crossId;
    }

    public String getCrossName() {
        Database db = new Database();
        Cross cross = db.readOneEntity(Cross.class, "id=" + this.crossId);
        return cross.getName();
    }
}
