package entity;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Dictionary;

public class User implements IDbEntity {
    Integer id = 0;
    String userName;
    String email;
    String password;
    Boolean isAdministrator;

    @Override
    public void loadData(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.userName = resultSet.getString("username");
        this.email = resultSet.getString("email");
        this.password = resultSet.getString("password");
        this.isAdministrator = resultSet.getBoolean("is_administrator");
    }

    @Override
    public void saveData(Dictionary<String, String> data) {
        data.put("username", this.userName);
        data.put("email", this.email);
        data.put("password", this.password);
        data.put("is_administrator", this.isAdministrator ? "1" : "0");
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAdministrator() {
        return isAdministrator;
    }

    public void setAdministrator(Boolean administrator) {
        isAdministrator = administrator;
    }

    public boolean comparePlainPassword(String plainPassword) {
        return this.password != null && this.password.equals(encodePassword(plainPassword));
    }

    public String encodePassword(String plainPassword) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                    plainPassword.getBytes(StandardCharsets.UTF_8));

            StringBuilder hexString = new StringBuilder();
            for (byte currentByte : encodedhash) {
                String hex = Integer.toHexString(0xff & currentByte);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return null;
    }
}
