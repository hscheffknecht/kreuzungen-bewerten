package entity;

import database.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Dictionary;

public class Cross implements IDbEntity {
    private Integer id = 0;
    private String name;
    private String description;
    private Integer nrOfRatings = 0;
    private Float averageRating = 0f;
    private String imageName;
    private Double latitude;
    private Double longitude;
    private Date createdOn;

    @Override
    public void loadData(ResultSet resultSet) throws SQLException {
        this.createdOn = resultSet.getDate("created_on");
        this.name = resultSet.getString("name");
        this.description = resultSet.getString("description");
        this.nrOfRatings = resultSet.getInt("nr_of_ratings");
        this.averageRating = resultSet.getFloat("average_rating");
        this.imageName = resultSet.getString("image_name");
        this.latitude = resultSet.getDouble("latitude");
        this.longitude = resultSet.getDouble("longitude");
    }

    public void saveData(Dictionary<String,String> data) {
        if (this.createdOn == null || this.createdOn.before(new Date(1971, 1, 1))) {
            this.createdOn = new Date();
        }

        data.put("created_on", (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).format(this.createdOn));
        data.put("name", this.name);
        data.put("description", this.description);
        data.put("nr_of_ratings", this.nrOfRatings.toString());
        data.put("average_rating", this.averageRating.toString());

        if (this.imageName != null) {
            data.put("image_name", this.imageName);
        }

        data.put("latitude", this.latitude.toString());
        data.put("longitude", this.longitude.toString());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNrOfRatings() {
        return nrOfRatings;
    }

    public void setNrOfRatings(Integer nrOfRatings) {
        this.nrOfRatings = nrOfRatings;
    }

    public Float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Float averageRating) {
        this.averageRating = averageRating;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void updateStatistics() {
        Database db = new Database();
        Statement statement = db.createStatement();

        try {
            ResultSet result = statement.executeQuery("SELECT count(*) as number, SUM(rating) as summary FROM ratings WHERE cross_id=" + this.id);
            result.next();

            this.nrOfRatings = result.getInt("number");
            this.averageRating = result.getFloat("summary") / this.nrOfRatings;

            result.close();
            statement.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public String getCreatedOnFormatted() {
        return (new SimpleDateFormat("dd.MM.yyyy")).format(this.createdOn);
    }

}
