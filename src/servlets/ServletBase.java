package servlets;

import database.Database;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

abstract public class ServletBase extends HttpServlet {
    protected void showView(HttpServletRequest request, HttpServletResponse response, String viewName, String headTitle) throws ServletException, IOException {
        boolean isLoggedIn = request.getSession().getAttribute("user_id") != null;

        request.setAttribute("head.title", headTitle);
        request.setAttribute("view.name", viewName);
        request.setAttribute("user_isLoggedIn", isLoggedIn);
        request.setAttribute("user_isAdministrator", this.getCurrentUser(request).getAdministrator());
        request.setAttribute("user_email", this.getCurrentUser(request).getEmail());

        request.getRequestDispatcher("page.jsp")
                .forward(request, response);
    }

    protected User getCurrentUser(HttpServletRequest request) {
        if (request.getSession().getAttribute("user_id") == null) {
            return new User();
        }

        int userId = (int)request.getSession().getAttribute("user_id");

        Database db = new Database();
        return db.readOneEntity(User.class, "id=" + userId);
    }
}
