package servlets;

import database.Database;
import entity.Rating;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/profile")
public class ProfileServlet extends ServletBase {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Check if current user is logged in
        if (request.getSession().getAttribute("user_id") == null) {
            response.sendRedirect("/login");
            return;
        }

        Database db = new Database();

        // Load ratings
        Rating[] ratings = db.readEntities(Rating.class, "user_id=" + request.getSession().getAttribute("user_id"), "created_on DESC");
        request.setAttribute("ratings", ratings);

        // Load current user
        User user = loadUser(request, db);
        request.setAttribute("currentUser", user);

        this.showView(request, response, "profile", "Mein Benutzerprofil");
    }

    private User loadUser(HttpServletRequest request, Database db) {
        return db.readOneEntity(User.class, "id=" + request.getSession().getAttribute("user_id"));
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        switch (action) {
            case "change_username":
                this.changeUsername(request);
                break;
            case "change_password":
                this.changePassword(request);
                break;
        }

        this.doGet(request, response);
    }

    private void changeUsername(HttpServletRequest request) {
        String newUserName = request.getParameter("username");
        if (newUserName.isEmpty()) {
            request.setAttribute("username_error", "Bitte geben Sie einen öffentlichen Benutzernamen an");
            return;
        }

        Database db = new Database();
        User user = loadUser(request, db);

        if (user.getId() == 0) {
            return;
        }

        user.setUserName(newUserName);
        db.save(user);

        request.setAttribute("username_success", "Die Änderung wurde erfolgreich gespeichert");
    }

    private void changePassword(HttpServletRequest request) {
        Database db = new Database();
        User user = loadUser(request, db);

        if (user.getId() == 0) {
            return;
        }

        String oldPassword = request.getParameter("password_old");
        if (oldPassword.isEmpty() || !user.comparePlainPassword(oldPassword)) {
            request.setAttribute("password_error", "Das aktuelle Passwort ist falsch");
            return;
        }

        String newPassword1 = request.getParameter("password_new1");
        String newPassword2 = request.getParameter("password_new2");
        if (newPassword1 == null || newPassword2 == null || newPassword1.isEmpty() || newPassword2.isEmpty()) {
            request.setAttribute("password_error", "Bitte geben Sie ein neues Passwort ein");
            return;
        }

        if (!newPassword1.equals(newPassword2)) {
            request.setAttribute("password_error", "Die neuen Passwörter stimmen nicht überein");
            return;
        }

        user.setPassword(user.encodePassword(newPassword1));
        db.save(user);

        request.setAttribute("password_success", "Die Änderung wurde erfolgreich gespeichert");
    }
}
