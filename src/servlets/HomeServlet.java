package servlets;

import database.Database;
import entity.Cross;
import entity.IDbEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("")
public class HomeServlet extends ServletBase {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Database db = new Database();

        Cross[] newCrosses = db.readEntities(Cross.class,"1=1", "created_on DESC");
        Cross[] popularCrosses = db.readEntities(Cross.class,"1=1", "nr_of_ratings DESC");
        Cross[] mapCrosses = db.readEntities(Cross.class, "1=1", "id");

        request.setAttribute("newCrosses", newCrosses);
        request.setAttribute("popularCrosses", popularCrosses);
        request.setAttribute("mapCrosses", mapCrosses);

        this.showView(request, response, "home", "Bewerte Kreuzungen um sie besser zu machen");
    }
}
