package servlets;

import database.Database;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends ServletBase {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.showView(request, response, "login", "Anmeldung");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        boolean success = false;

        switch (action) {
            case "login":
                success = login(request);
                break;
            case "register":
                success = register(request);
                break;
        }

        if (success) {
            response.sendRedirect("/");
        } else {
            this.doGet(request, response);
        }
    }

    private boolean login(HttpServletRequest request) {
        User user = loadUserByEmail(request.getParameter("email"));
        if (!user.comparePlainPassword(request.getParameter("password"))) {
            request.setAttribute("login_error", "Die Kombination aus E-Mail Adresse und Passwort ist falsch");
            return false;
        }

        // Login
        doLogin(user, request.getSession());
        return true;
    }

    private boolean register(HttpServletRequest request) {
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");

        if (password1 == null || !password1.equals(password2)) {
            request.setAttribute("register_error", "Die Passwörter stimmen nicht überein");
            return false;
        }

        String email = request.getParameter("email");
        User user = loadUserByEmail(email);

        if (user.getId() > 0) {
            request.setAttribute("register_error", "Es existiert bereits ein Benutzer mit dieser E-Mail Adresse");
            return false;
        }

        user.setUserName(request.getParameter("username"));
        user.setEmail(email);
        user.setPassword(user.encodePassword(password1));
        user.setAdministrator(false);

        Database db = new Database();
        boolean success = db.save(user);

        if (success) {
            doLogin(user, request.getSession());
        }

        return success;
    }

    private void doLogin(User user, HttpSession session) {
        session.setAttribute("user_id", user.getId());
    }

    private User loadUserByEmail(String email) {
        Database db = new Database();
        return db.readOneEntity(User.class, "email='" + email.replace("'", "''") + "'");
    }
}
