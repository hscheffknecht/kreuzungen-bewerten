package servlets;

import database.Database;
import entity.Cross;
import entity.Rating;
import entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

@WebServlet("/edit")
@MultipartConfig
public class EditServlet extends ServletBase {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cross cross = loadCross(request);

        if (!checkPermission(request, response, cross)) {
            return;
        }

        request.setAttribute("cross", cross);

        this.showView(request, response, "edit", cross.getId() == 0 ? "Neue Kreuzung erstellen" : "\"" + cross.getName() + "\" bearbeiten");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Cross cross = loadCross(request);

        if (!checkPermission(request, response, cross)) {
            return;
        }

        cross.setName(request.getParameter("name"));
        cross.setDescription(request.getParameter("description"));
        cross.setLatitude(Double.parseDouble(request.getParameter("lat")));
        cross.setLongitude(Double.parseDouble(request.getParameter("lng")));

        // Upload file
        this.handleFileUpload(request, cross);

        Database database = new Database();
        database.save(cross);

        response.sendRedirect("detail?id=" + cross.getId());
    }

    private void handleFileUpload(HttpServletRequest request, Cross cross) throws IOException, ServletException {
        Random random = new Random();
        Part image = request.getPart("image");

        if (image.getSize() == 0) {
            return;
        }

        String filename = random.nextInt(9999999) + "-" + image.getSubmittedFileName();
        String fullFilename = this.getServletContext().getRealPath("images/crosses/" + filename);

        image.write(fullFilename);
        cross.setImageName(filename);
    }

    private Cross loadCross(HttpServletRequest request) {
        Cross cross = new Cross();
        String idParameter = request.getParameter("id");

        if (idParameter != null) {
            Database db = new Database();

            cross = db.readOneEntity(Cross.class, "id=" + Integer.parseInt(idParameter));
        }

        return cross;
    }

    private boolean checkPermission(HttpServletRequest request, HttpServletResponse response, Cross cross) throws IOException {
        User user = this.getCurrentUser(request);
        if (user.getId() == 0) {
            response.sendRedirect("/"); // Not logged in
            return false;
        }

        if (cross.getId() > 0 && !user.getAdministrator()) {
            response.sendRedirect("/edit"); // Not a administrator, is only allowed to create new crosses
            return false;
        }

        return true;
    }
}
