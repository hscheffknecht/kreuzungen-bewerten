package servlets;

import database.Database;
import entity.Cross;
import entity.Rating;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

@WebServlet("/detail")
public class DetailServlet extends ServletBase {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Database db = new Database();

        Cross cross = db.readOneEntity(Cross.class, "id=" + Integer.parseInt(request.getParameter("id")));
        Rating[] ratings = db.readEntities(Rating.class,"cross_id=" + cross.getId(), "created_on DESC");
        Rating myRating = loadMyRating(db, cross.getId(), getUserId(request.getSession()));

        request.setAttribute("cross", cross);
        request.setAttribute("ratings", ratings);
        request.setAttribute("hasRatings", ratings.length > 0);
        request.setAttribute("myRating", myRating);

        this.showView(request, response, "detail", cross.getName());
    }

    private Rating loadMyRating(Database db, Integer crossId, int userId) {
        return db.readOneEntity(Rating.class, "cross_id=" + crossId + " AND user_id=" + userId);
    }

    private int getUserId(HttpSession session) {
        Object rawUserId = session.getAttribute("user_id");
        return rawUserId != null ? (int)rawUserId : 0;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        Database db = new Database();
        int crossId = Integer.parseInt(request.getParameter("id"));
        int userId = getUserId(request.getSession());

        Rating myRating = loadMyRating(db, crossId, userId);
        boolean isNewRating = myRating.getId() == 0;

        if (isNewRating) {
            myRating.setUserId(userId);
        }

        myRating.setCreatedOn(new Date());
        myRating.setCrossId(crossId);
        myRating.setRating(Integer.parseInt(request.getParameter("rating")));
        myRating.setText(request.getParameter("text"));
        db.save(myRating);

        // Update cross statistics
        Cross cross = db.readOneEntity(Cross.class, "id=" + Integer.parseInt(request.getParameter("id")));
        cross.updateStatistics();
        db.save(cross);

        this.doGet(request, response);
    }
}
