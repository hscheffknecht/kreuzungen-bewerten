package database;

import entity.IDbEntity;

import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.*;

public class Database {

    public final String DB_DRIVER = "com.mysql.jdbc.Driver";

    private static Connection connection;

    public <T extends IDbEntity> T readOneEntity(Class<T> entityClass, String where) {
        T[] list = this.readEntities(entityClass, where, null);
        if (list.length == 0) {
            try {
                return entityClass.newInstance();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        return list[0];
    }

    public <T extends IDbEntity> T[] readEntities(Class<T> entityClass, String where, String order) {
        Connect();

        ResultSet resultSet;
        T entity;
        Statement statement;
        List<T> list = new ArrayList<T>();

        try {
            statement = Database.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

            String query = "SELECT * FROM " + this.convertEntityToTableName(entityClass) + " WHERE " + where;

            if (order != null) {
                query += " ORDER BY " + order;
            }

            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                entity = entityClass.newInstance();

                entity.loadData(resultSet);
                entity.setId(resultSet.getInt("id"));

                list.add(entity);
            }

            resultSet.close();
            statement.close();

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return list.toArray((T[]) java.lang.reflect.Array.newInstance(entityClass, list.size()));
    }

    public Boolean save(IDbEntity dbEntity) {
        Hashtable<String, String> data = new Hashtable<>();
        dbEntity.saveData(data);
        String table = this.convertEntityToTableName(dbEntity.getClass());
        String query;

        if (dbEntity.getId() == 0) {
            // Insert
            StringBuilder fields = new StringBuilder();
            StringBuilder values = new StringBuilder();

            boolean first = true;

            for (String key : data.keySet()) {
                if (first) {
                    first = false;
                } else {
                    fields.append(',');
                    values.append(',');
                }

                fields.append(key);

                values.append("'");
                values.append(data.get(key).replace("'", "''"));
                values.append("'");
            }


            query = "INSERT INTO " + table + " (" + fields.toString() + ") VALUES (" + values.toString() + ")";
        } else {
            // Update
            StringBuilder queryBuilder = new StringBuilder("UPDATE " + table + " SET ");

            boolean first = true;

            for (String key : data.keySet()) {
                if (first) {
                    first = false;
                } else {
                    queryBuilder.append(',');
                }

                queryBuilder.append(key);
                queryBuilder.append("='");
                queryBuilder.append(data.get(key));
                queryBuilder.append("'");
            }

            queryBuilder.append(" WHERE id=");
            queryBuilder.append(dbEntity.getId());
            query =queryBuilder.toString();
        }

        try {
            Statement statement = Database.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statement.execute(query);

            if (dbEntity.getId() == 0) {
                ResultSet resultSet = statement.executeQuery("SELECT id FROM " + table + " ORDER BY id DESC LIMIT 1");
                resultSet.next();
                dbEntity.setId(resultSet.getInt("id"));
            }
            return true;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

        return false;
    }

    public Statement createStatement() {
        Statement statement = null;

        try {
            statement = Database.connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return statement;
    }

    private void Connect() {
        if (Database.connection != null) {
            return;
        }

        try {
            String url = "jdbc:mysql://" + System.getProperty("RDS_HOSTNAME") + ":" + System.getProperty("RDS_PORT") + "/" + System.getProperty("RDS_DB_NAME");

            Class.forName(DB_DRIVER);
            Database.connection = DriverManager.getConnection(url, System.getProperty("RDS_USERNAME"), System.getProperty("RDS_PASSWORD"));
        }
        catch (ClassNotFoundException exception) {
            System.err.println("Failed to load the driver for the database: " + exception.getMessage());
        }
        catch (SQLException exception) {
            System.err.println(exception.getMessage());
        }
    }

    private String convertEntityToTableName(Class entityClass) {
        String tableName = entityClass.getName().replace("entity.", "").toLowerCase();

        switch (tableName) {
            case "cross":
                tableName = "crosses";
                break;
            default:
                tableName += "s";
        }

        return tableName;
    }
}
