<%@ page import="entity.Cross" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%--<div class="jumbotron my-3">
    <form>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Nach Kreuzungen suchen">
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button" id="button-addon2" onclick="document.location.href='/detail?id=1';">Suchen</button>
            </div>
        </div>
    </form>
</div>--%>

<div class="mb-3">
    <div id="map"></div>
</div>

<div class="row mb-3">
    <div class="col-md">
        <table class="table">
            <colgroup>
                <col width="100px">
            </colgroup>
            <thead class="thead-light">
            <tr>
                <th scope="col" colspan="2">Neueste Kreuzungen</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${newCrosses}" var="cross">
            <tr>
                <th scope="row">${cross.createdOnFormatted}</th>
                <td><a href="/detail?id=${cross.id}">${cross.name}</a></td>
            </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <div class="col-md">
        <table class="table">
            <colgroup>
                <col width="170px">
            </colgroup>
            <thead class="thead-light">
            <tr>
                <th scope="col" colspan="2">Kreuzungen über die gesprochen wird</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${popularCrosses}" var="cross">
                <tr>
                    <th scope="row">${cross.nrOfRatings} Bewertungen</th>
                    <td><a href="/detail?id=${cross.id}">${cross.name}</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>


<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: {
                lat: 47.607798,
                lng: 13.300144
            }
        });

        var infowindow = new google.maps.InfoWindow();

        // Geolocation
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                map.setCenter(pos);
                map.setZoom(9);
            }, function() {
                console.error('geolocation error');
            });
        }

        <c:forEach items="${newCrosses}" var="cross">
        var marker${cross.id} = new google.maps.Marker({
            position: {
                lat: ${cross.latitude},
                lng: ${cross.longitude}
            },
            map: map,
            title: '${cross.name}'
        });

        marker${cross.id}.addListener('click', function() {
            infowindow.open(map, marker${cross.id});
            infowindow.setContent('<a href="/detail?id=${cross.id}">${fn:replace(cross.name,"'", "\\'")}</a>');
        });
        </c:forEach>
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA78HUHnY54dlv3YqzJeTA4SQYDsCkSrz4&callback=initMap">
</script>
