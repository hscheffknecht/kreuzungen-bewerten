<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<h1 class="my-3 text-center"><c:if test="${cross.id == 0}">Neue Kreuzung anlegen</c:if><c:if test="${cross.id > 0}">"${cross.name}" bearbeiten</c:if></h1>
<form method="post" enctype="multipart/form-data">
    <div class="row bg-light p-3 mb-3">
        <div class="col-md">
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" type="text" id="name" name="name" value="${cross.name}">
            </div>
            <div class="form-group">
                <label for="description">Beschreibung</label>
                <textarea class="form-control" style="min-height: 300px;" id="description" name="description">${cross.description}</textarea>
            </div>
            <c:if test="${cross.id > 0}">
            <div class="form-group">
                <img src="/images/crosses/${cross.imageName}" style="width: 100%">
            </div>
            </c:if>
            <div class="form-group">
                <label for="image">Bild hochladen</label>
                <div><input id="image" name="image" type="file"></div>
            </div>

        </div>
        <div class="col-md">
            <div class="form-group">
                <label>Position wählen</label>
                <div id="map"></div>
                <input id="lat" type="hidden" name="lat">
                <input id="lng" type="hidden" name="lng">
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <c:if test="${cross.id > 0}">
            <button class="btn btn-link">Zurück</button>
        </c:if>
        <button class="btn btn-primary" type="submit">Speichern</button>
    </div>
</form>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA78HUHnY54dlv3YqzJeTA4SQYDsCkSrz4"></script>
<script src="/lib/location-picker.min.js"></script>

<script>
  var lp = new locationPicker('map', {
    <c:if test="${cross.id > 0}">
        lat: ${cross.latitude},
        lng: ${cross.longitude}
    </c:if>
    <c:if test="${cross.id == 0}">
        lat: 48.210033, // Use vienna as default when no location is available
        lng: 16.363449,
        setCurrentPosition: true
    </c:if>
  });

  var latHidden = document.getElementById('lat');
  var lngHidden = document.getElementById('lng');

  // Set changes to hidden field
  google.maps.event.addListener(lp.map, 'idle', function (event) {
    var location = lp.getMarkerPosition();

    latHidden.value = location.lat;
    lngHidden.value = location.lng;
  });
</script>