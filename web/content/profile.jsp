<%--
  Created by IntelliJ IDEA.
  User: herbert
  Date: 04.12.18
  Time: 21:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<h1 class="my-4 text-center">Mein Benutzerprofil</h1>
<div class="row">
    <div class="col-md text-truncate">
        <div class="bg-light p-4 mb-3">
            <h2>Meine Bewertungen</h2>
            <c:forEach items="${ratings}" var="rating">
                <a href="/detail?id=${rating.crossId}">${rating.crossName}</a>
                <div class="text-truncate">
                    <small>${rating.text}</small>
                </div>
                <hr>
            </c:forEach>
        </div>
    </div>
    <div class="col-md">
        <form class="bg-light p-4 mb-3" method="post">
            <input type="hidden" name="action" value="change_username">
            <h2>Meine Daten</h2>
            <div class="form-group">
                <label for="email">Ihre E-Mail Adresse</label>
                <input class="form-control" id="email" disabled value="${currentUser.email}">
            </div>
            <div class="form-group">
                <label for="username">Öffentlicher Benutzername</label>
                <input class="form-control" id="username" name="username" value="${currentUser.userName}" required>
            </div>
            <c:if test="${username_error != null}">
                <p class="text-danger">${username_error}</p>
            </c:if>
            <c:if test="${username_success != null}">
                <p class="text-success">${username_success}</p>
            </c:if>
            <button class="btn btn-primary">Daten speichern</button>
        </form>

        <form class="bg-light p-4 mb-3" method="post">
            <input type="hidden" name="action" value="change_password">
            <h2>Passwort ändern</h2>
            <div class="form-group">
                <label for="password_old">Altes Passwort</label>
                <input id="password_old" name="password_old" class="form-control" type="password" required>
            </div>
            <div class="form-group">
                <label for="password_new1">Neues Passwort</label>
                <input id="password_new1" name="password_new1" class="form-control" type="password" required>
            </div>
            <div class="form-group">
                <label for="password_new2">Neues Passwort wiederholen</label>
                <input id="password_new2" name="password_new2" class="form-control" type="password" required>
            </div>
            <c:if test="${password_error != null}">
                <p class="text-danger">${password_error}</p>
            </c:if>
            <c:if test="${password_success != null}">
                <p class="text-success">${password_success}</p>
            </c:if>
            <button class="btn btn-primary">Passwort ändern</button>
        </form>
    </div>
</div>