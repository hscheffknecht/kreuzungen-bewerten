<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<h1 class="my-3 text-center">${fn:escapeXml(cross.name)}</h1>
<c:if test="${user_isAdministrator}">
    <p class="text-center">
        <a href="/edit?id=${cross.id}">Kreuzung bearbeiten</a>
    </p>
</c:if>
<p class="text-center">
    <span class="rating">
        <jsp:include page="../includes/rating.jsp">
            <jsp:param name="rating" value="${cross.averageRating}"/>
        </jsp:include>
    </span>
    ${cross.nrOfRatings} Bewertungen
</p>
<div class="row bg-light p-3 mb-3">
    <div class="col-md">
        <p class="pre">${fn:escapeXml(cross.description)}</p>
        <p>
            <img src="/images/crosses/${cross.imageName}" style="width: 100%">
        </p>
        <div id="map"></div>
    </div>
    <div class="col-md">
        <h2>Bewertungen</h2>
        <c:forEach items="${ratings}" var="rating">
            <p>
                <span class="rating">
                    <jsp:include page="../includes/rating.jsp">
                        <jsp:param name="rating" value="${rating.rating}" />
                    </jsp:include>
                </span>
                <strong>${fn:escapeXml(rating.userName)}</strong> am ${rating.createdOnFormatted}:<br>
                <div class="pre">${fn:escapeXml(rating.text)}</div>
            </p>
        </c:forEach>

        <c:if test="${!hasRatings}">
            <p>Bisher wurden keine Bewertungen verfasst.</p>
        </c:if>

        <h2 class="mt-5 mb-3">Eigene Bewertung verfassen</h2>
        <c:if test="${user_isLoggedIn}">
            <form method="post">
                <div class="form-group">
                    <label for="rating">Ihre Bewertung:</label>
                    <select class="form-control" id="rating" name="rating">
                        <option>Bitte wählen…</option>
                        <option value="1" <c:if test="${myRating.rating == 1}">selected</c:if>>1 Stern</option>
                        <option value="2" <c:if test="${myRating.rating == 2}">selected</c:if>>2 Sterne</option>
                        <option value="3" <c:if test="${myRating.rating == 3}">selected</c:if>>3 Sterne</option>
                        <option value="4" <c:if test="${myRating.rating == 4}">selected</c:if>>4 Sterne</option>
                        <option value="5" <c:if test="${myRating.rating == 5}">selected</c:if>>5 Sterne</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="text">Ihre Rezension:</label>
                    <textarea class="form-control" style="min-height: 150px;" id="text" name="text">${myRating.text}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Bewertung <c:if test="${myRating.id == 0}">veröffentlichen</c:if><c:if test="${myRating.id > 0}">ändern</c:if></button>
                </div>
            </form>
        </c:if>
        <c:if test="${!user_isLoggedIn}">
            <p><a href="/login">Bitte melden Sie sich an, um eine Bewertung zu verfassen.</a></p>
        </c:if>
    </div>
</div>

<script>
      function initMap() {
        var myLatLng = {lat: ${cross.latitude}, lng: ${cross.longitude}};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: '${cross.name}'
        });
      }
    </script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA78HUHnY54dlv3YqzJeTA4SQYDsCkSrz4&callback=initMap">
</script>
