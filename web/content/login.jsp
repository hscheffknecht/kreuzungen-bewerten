<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<h1 class="text-center my-3">Anmeldung</h1>

<div class="row bg-light p-3">
    <div class="col-md">
        <h2>Login</h2>

        <form method="post">
            <input type="hidden" name="action" value="login">
            <div class="form-group">
                <label for="login_email">E-Mail Adresse</label>
                <input type="text" class="form-control" id="login_email" name="email" placeholder="E-Mail Adresse">
            </div>
            <div class="form-group">
                <label for="login_password">Passwort</label>
                <input type="password" class="form-control" id="login_password" name="password" placeholder="Passwort">
            </div>
            <div class="form-group">
                <c:if test="${login_error != null}">
                    <p class="text-danger">${login_error}</p>
                </c:if>
                <button class="btn btn-primary">Abschicken</button>
            </div>
        </form>
    </div>
    <div class="col-md">
        <h2>Neu registrieren</h2>

        <form method="post">
            <input type="hidden" name="action" value="register">
            <div class="form-group">
                <label for="register_email">E-Mail Adresse</label>
                <input type="text" class="form-control" placeholder="Benutzername" id="register_email" name="email" required>
            </div>
            <div class="form-group">
                <label for="register_username">Öffentlicher Benutzername</label>
                <input type="text" class="form-control" placeholder="Benutzername" id="register_username" name="username" required>
            </div>
            <div class="form-group">
                <label for="register_password">Passwort</label>
                <input type="password" class="form-control" placeholder="Passwort" id="register_password" name="password1" required>
            </div>
            <div class="form-group">
                <label for="register_password2">Passwort wiederholen</label>
                <input type="password" class="form-control" placeholder="Passwort wiederholen" id="register_password2" name="password2" required>
            </div>
            <div class="form-group">
                <c:if test="${register_error != null}">
                    <p class="text-danger">${register_error}</p>
                </c:if>
                <button class="btn btn-primary">Abschicken</button>
            </div>
        </form>
    </div>
</div>