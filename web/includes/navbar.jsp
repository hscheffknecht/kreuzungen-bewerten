<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<nav class="navbar navbar-expand-lg navbar-light bg-light my-3">
    <a class="navbar-brand" href="/">kreuzungen-bewerten.at</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-between" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <c:if test="${user_isLoggedIn}">
                <li class="nav-item">
                    <a class="nav-link" href="/edit">Neue Kreuzung anlegen</a>
                </li>
            </c:if>
        </ul>
        <ul class="navbar-nav">
            <c:if test="${user_isLoggedIn}">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Angemeldet als: ${user_email}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/profile">Mein Profil</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="/logout">Abmelden</a>
                    </div>
                </li>
            </c:if>
            <c:if test="${!user_isLoggedIn}">
                <li class="nav-item">
                    <a class="nav-link" href="/login">Anmelden</a>
                </li>
            </c:if>
        </ul>
    </div>
</nav>