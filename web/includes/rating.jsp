<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<%
    float rating = Float.parseFloat(request.getParameter("rating"));
    request.setAttribute("rating", rating);
%>

<c:if test="${rating == 0}">
    <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i>
</c:if>
<c:if test="${rating > 0 && rating < 1.25}">
    <i class="fas fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i>
</c:if>
<c:if test="${rating >= 1.25 && rating < 1.75}">
    <i class="fas fa-star"></i> <i class="fas fa-star-half-alt"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i>
</c:if>
<c:if test="${rating >= 1.75 && rating < 2.25}">
    <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i>
</c:if>
<c:if test="${rating >= 2.25 && rating < 2.75}">
    <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star-half-alt"></i> <i class="far fa-star"></i> <i class="far fa-star"></i>
</c:if>
<c:if test="${rating >= 2.75 && rating < 3.25}">
    <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="far fa-star"></i> <i class="far fa-star"></i>
</c:if>
<c:if test="${rating >= 3.25 && rating < 3.75}">
    <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star-half-alt"></i> <i class="far fa-star"></i>
</c:if>
<c:if test="${rating >= 3.75 && rating < 4.25}">
    <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="far fa-star"></i>
</c:if>
<c:if test="${rating >= 4.25 && rating < 4.75}">
    <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star-half-alt"></i>
</c:if>
<c:if test="${rating >= 4.75}">
    <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i>
</c:if>
