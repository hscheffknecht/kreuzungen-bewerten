<%@ page contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html lang="de">
  <%@ include file="includes/head.jsp" %>
  <body>
    <div class="container">
      <%@ include file="includes/navbar.jsp" %>
      <% pageContext.include("content/" + request.getAttribute("view.name") + ".jsp"); %>
    </div>

    <%@ include file="includes/body_end.jsp" %>
  </body>
</html>