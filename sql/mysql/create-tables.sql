CREATE TABLE users
(
    id int PRIMARY KEY AUTO_INCREMENT,
    username varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    is_administrator boolean DEFAULT false NOT NULL
);
CREATE UNIQUE INDEX users_email_uindex ON users (email);

CREATE TABLE crosses
(
    id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    description longtext,
    nr_of_ratings int,
    average_rating double,
    image_name varchar(255),
    latitude double,
    longitude double,
    created_on timestamp
);

CREATE TABLE ratings
(
    id int PRIMARY KEY AUTO_INCREMENT,
    user_id int NOT NULL,
    created_on timestamp NOT NULL,
    text longtext NOT NULL,
    rating int NOT NULL,
    cross_id int NOT NULL,
    CONSTRAINT ratings_crosses_id_fk FOREIGN KEY (cross_id) REFERENCES crosses (id),
    CONSTRAINT ratings_users_id_fk FOREIGN KEY (user_id) REFERENCES users (id)
);
