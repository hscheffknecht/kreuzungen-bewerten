INSERT INTO users (id, username, email, password, is_administrator) VALUES (1, 'haby83', 'herbert.scheffknecht@gmail.com', '37268335dd6931045bdcdf92623ff819a64244b53d0e746d438797349d4da578', true);

INSERT INTO crosses (id, name, description, nr_of_ratings, average_rating, image_name, latitude, longitude, created_on) VALUES (1, 'Ampelkreuzung Zubringer Feldkirch-Frastanz ', 'Das ist die berüchtigte Kreuzung, die das Portal veranlasst hat. 😀', 2, 3.5, '3.jpg', 47.22489428907176, 9.608426666517175, '2018-12-27 02:42:48.000000000');

INSERT INTO ratings (id, user_id, created_on, text, rating, cross_id) VALUES (1, 1, '2018-12-27 14:28:56.000000000', 'Ist immer noch ziemlich mies. 😡', 3, 1);
